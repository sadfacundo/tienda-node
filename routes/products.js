const express = require('express');
const ProductsService = require('../services/productsService');



const router = express.Router();
service = new ProductsService;

router.get('/', (req, res) => {
  const products = service.find();


  res.json(products);


});

router.get('/:id', (req, res) => {
  const {id} = req.params;

  const product = service.findOne(id);


  res.json(product);


});



router.post('/', (req, res) => {
  const body = req.body;
  res.json({
    message:"Created",
    data:body
  });
});

router.put('/:id', (req, res) => {
  const body = req.body;
  const {id} = req.params;

  res.json({
    id,
    message:"Updated",
    data:body
  });
});

router.delete('/:id', (req, res) => {
  const body = req.body;
  const {id} = req.params;
  res.json({
    id,
    message:"Deleted",
    data:body
  });
});






module.exports = router;
